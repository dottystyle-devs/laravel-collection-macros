<?php

namespace Dottystyle\LaravelCollectionMacros\Contracts;

interface HasOptionableDataMerge
{
    /**
     * Get the data to merge to options.
     * 
     * @return array
     */
    public function getOptionableDataMerge();
}