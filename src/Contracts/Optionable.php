<?php

namespace Dottystyle\LaravelCollectionMacros\Contracts;

interface Optionable
{
    /**
     * Get the label for options.
     * 
     * @return string
     */
    public function getOptionLabel();

    /**
     * Get the label for value.
     * 
     * @return mixed
     */
    public function getOptionValue();
}