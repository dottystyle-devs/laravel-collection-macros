<?php

namespace Dottystyle\LaravelCollectionMacros;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class CollectionMacrosServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     * 
     * @return void
     */
    public function boot()
    {
        foreach ($this->macros() as $method => $invokable) {
            // Skip if a macro has already been registered
            if (! Collection::hasMacro($method)) {
                Collection::macro($method, $this->app->make($invokable)());
            }
        }
    }

    /**
     * Get the available macros.
     * 
     * @return array
     */
    protected function macros()
    {
        return [
            'toOptions' => Macros\Options::class
        ];
    }
}