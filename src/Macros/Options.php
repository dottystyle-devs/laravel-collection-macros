<?php

namespace Dottystyle\LaravelCollectionMacros\Macros;

use Closure;
use Dottystyle\LaravelCollectionMacros\Contracts\HasOptionableDataMerge;
use Dottystyle\LaravelCollectionMacros\Contracts\Optionable;

class Options
{
    /**
     * @var string
     */
    protected static $valueKey = 'value';

    /**
     * @var string
     */
    protected static $labelKey = 'label';

    /**
     * Set the label and value keys.
     * 
     * @param string $value
     * @param string $label
     * @return void
     */
    public static function setLabelAndValueKeys($label, $value)
    {
        self::$valueKey = $value;
        self::$labelKey = $label;
    }
    
    /**
     * Get the key to use for labels.
     * 
     * @return string
     */
    public function getLabelKey()
    {
        return self::$labelKey;
    }

    /**
     * Get the key to use for value.
     * 
     * @return string
     */
    public function getValueKey()
    {
        return self::$valueKey;
    }

    /**
     * Set the label and value keys temporarily, reverting back to the previous 
     * after the callback.
     * 
     * @param string $label
     * @param string $value
     * @param callable $callBack
     * @return void
     */
    public static function usingLabelAndValueKeys($label, $value, $callBack)
    {
        $previousLabelKey = self::$labelKey;
        $previousValueKey = self::$valueKey;

        self::setLabelAndValueKeys($label, $value);
        
        call_user_func($callBack);

        self::setLabelAndValueKeys($previousLabelKey, $previousValueKey);
    }

    /**
     * Map each item as collection options.
     * 
     * @return mixed
     */
    public function __invoke()
    { 
        $that = $this;

        return function ($label = null, $value = null) use ($that) {
            $options = is_array($label) 
                ? $that->normalizeOptions($label)
                : $that->normalizeOptions(compact('label', 'value'));

            return $this->map(function ($item, $key) use ($options, $that) {
                return $that->mergeIfAny($item, $key, $options, [
                    $options['label_key'] => $that->getLabel($item, $options['label'], $key),
                    $options['value_key'] => $that->getValue($item, $options['value'], $key)
                ]);
            });
        };        
    }

    /**
     * Get the option label for the item.
     * 
     * @param mixed $item
     * @param string $label
     * @param mixed $key
     * @return string
     */
    public function getLabel($item, $label, $key)
    {
        if (isset($label)) {
            return $this->value($item, $label, $key, $key);
        }

        if ($item instanceof Optionable) {
            return $item->getOptionLabel();
        }
        
        return is_scalar($item) ? $item : $key;
    }

    /**
     * Get the option value for the item.
     * 
     * @param mixed $item
     * @param string $value
     * @param string $key
     * @return mixed
     */
    public function getValue($item, $value, $key)
    {
        if (isset($value)) {
            return $this->value($item, $value, $key, $key);
        }

        return $item instanceof Optionable ? $item->getOptionValue() : $key;
    }
    
    /**
     * Resolve the value of prop from the item.
     * 
     * @param mixed $item
     * @param mixed $prop
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
    protected function value($item, $prop, $key, $default)
    {
        return $prop instanceof Closure
                ? call_user_func($prop, $item, $key, $default)
                : data_get($item, $prop, $default);
    }

    /**
     * Merge additional/extra data.
     * 
     * @param mixed $item
     * @param mixed $key
     * @param array $options
     * @param array $data
     * @return array
     */
    public function mergeIfAny($item, $key, array $options, array $data)
    {
        $merge = data_get($options, 'merge', null);

        if ($merge instanceof Closure) {
            return call_user_func($merge, $data, $item, $key, $options);
        }

        if ($item instanceof HasOptionableDataMerge && $merge !== false) {
            return array_merge($data, $item->getOptionableDataMerge());
        }

        return $data;
    }

    /**
     * Normalize the options for options macro.
     * 
     * @param array $options
     * @return array
     */
    public function normalizeOptions(array $options)
    {
        return array_merge([
            'label_key' => $this->getLabelKey(),
            'value_key' => $this->getValueKey(),
            'label' => null,
            'value' => null,
            'merge' => null,
            'default' => ''
        ], $options);
    }
}